// core
import { FC } from 'react';

export const Footer: FC = () => {
    return (
        <footer>
            <span>@ 2023 Lectrum LLC - Все права защищены.</span>
        </footer>
    );
};

