// core
import { FC } from 'react';

// elements
import { SignUpForm } from '../forms';

export const SignUp: FC = () => {
    return (
        <section className = 'publish-tip login-form'>
            <SignUpForm />
        </section>
    );
};

