// core
import { FC } from 'react';

export const Spinner: FC = () => {
    return (
        <div className = 'spinner'>
            <div aria-busy = { true }>
                <div className = 'react-spinner-loader-svg'>
                    <svg
                        id = 'triangle' viewBox = '-3 -4 39 39'
                        aria-label = 'audio-loading'>
                        <polygon
                            fill = 'transparent' stroke = '#FE4D97'
                            strokeWidth = '1' points = '16,0 32,32 0,32'></polygon>
                    </svg>
                </div>
            </div>
        </div>
    );
};

