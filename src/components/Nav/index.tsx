// core
import React from 'react';
import { NavLink, useNavigate } from 'react-router-dom';

// hooks
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/reduxTypes';

// types
import { status, ToastOptions } from '../../types';

// actions && selectors
import { authActions } from '../../lib/redux/actions';
import { tokenSelector } from '../../lib/redux/selectors';

// helpers
import { book } from '../../book';
import { toastConstants, toastMessages } from '../../constants';
import { changeAuthorizationStatus, createToasters } from '../../helpers';

export const Nav: React.FC = () => {
    const token = useAppSelector(tokenSelector);
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const logOutHandler = () => {
        dispatch(authActions.setUserProfileData(null));
        changeAuthorizationStatus({ status: status.SET_UNAUTHORIZED, dispatch, token: null });
        createToasters(ToastOptions.info, toastMessages({
            name: toastConstants.QUIT_MESSAGE,
        }));
        navigate(book.login);
    };

    return (
        <nav>
            { token && (
                <>
                    <NavLink to = { book.taskManager }>К задачам</NavLink>
                    <NavLink to = { book.profile }>Профиль</NavLink>
                    <button
                        className = 'button-logout'
                        onClick = { logOutHandler }> Выйти
                    </button>
                </>
            ) }
        </nav>
    );
};
