/* eslint-disable no-template-curly-in-string */
// core
import * as yup from 'yup';

// types
import { ToDoFormType } from '../../../types';


export const schema: yup.SchemaOf<ToDoFormType> = yup.object().shape({
    title:       yup.string().required('*').min(3, 'Минимальная длина title - ${min}').max(64, 'Максимальная длина title - ${max}'),
    deadline:    yup.string().required('*'),
    description: yup.string().required('*').min(3, 'Минимальная длина description - ${min}').max(150, 'Максимальная длина description - ${max}'),
});
