// core
import { FC } from 'react';

// hooks
import { useAppDispatch, useAppSelector } from '../../../../lib/redux/init/reduxTypes';

// actions && selectors
import { tasksActions } from '../../../../lib/redux/actions';
import { selectedTaskSelector } from '../../../../lib/redux/selectors';
import { ToastOptions } from '../../../../types';

// helpers
import { toastConstants, toastMessages } from '../../../../constants';
import { createToasters } from '../../../../helpers';

export const Head: FC = () => {
    const selectedTask = useAppSelector(selectedTaskSelector);
    const dispatch = useAppDispatch();

    const setToDoComplete = (): void => {
        dispatch(tasksActions.setToDoComplete(true));
        // createToasters(ToastOptions.success, toastMessages({
        //     name: toastConstants.COMPLETE_TASK_MESSAGE,
        // }));
    };

    const deleteToDoRequest = (): void => {
        dispatch(tasksActions.deleteToDoRequest(selectedTask?.id ? selectedTask?.id : ''));
        dispatch(tasksActions.setIsNewTaskFormOpen(false));
        createToasters(ToastOptions.info, toastMessages({
            name: toastConstants.DELETE_TASK_MESSAGE,
        }));
    };

    return (
        <div className = 'head'>
            { selectedTask && (
                <>
                    <button
                        onClick = { setToDoComplete }
                        className = 'button-complete-task'> завершить
                    </button>
                    <div
                        onClick = { deleteToDoRequest }
                        className = 'button-remove-task'>
                    </div>
                </>
            ) }
        </div>
    );
};

