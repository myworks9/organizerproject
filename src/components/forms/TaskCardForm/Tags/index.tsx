// core
import { FC, ReactElement } from 'react';
import { fetchify } from '../../../../helpers';

// hooks
import { useAppSelector } from '../../../../lib/redux/init/reduxTypes';

// selector
import { isFetchingSelector, tagsSelector } from '../../../../lib/redux/selectors';

// components
import { Tag } from './Tag';

export const Tags: FC = () => {
    const tags = useAppSelector(tagsSelector);
    const isFetching = useAppSelector(isFetchingSelector);

    const tagsJSX = tags.map(({
        id, name, bg, color,
    }): ReactElement => <Tag
        key = { id }
        id = { id }
        name = { name }
        color = { color }
        bg = { bg } />);

    return (
        <div className = 'tags'>
            { fetchify(isFetching, tagsJSX) }
        </div>
    );
};
