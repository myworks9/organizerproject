// core
import cx from 'classnames';
import { FC } from 'react';

// hooks
import { useAppDispatch, useAppSelector } from '../../../../../lib/redux/init/reduxTypes';

// actions && selectors
import { tasksActions } from '../../../../../lib/redux/actions';
import { selectedTagIdForRequestSelector } from '../../../../../lib/redux/selectors';
import { TagsResponseType } from '../../../../../types';

export const Tag: FC<TagsResponseType> = ({
    color, bg, name, id,
}) => {
    const selectedTagIdForRequest = useAppSelector(selectedTagIdForRequestSelector);
    const dispatch = useAppDispatch();

    const selectedTag: string = cx('tag', {
        selected: selectedTagIdForRequest === id,
    });

    const setSelectedTagIdForRequestHandler = (): void => {
        if (selectedTagIdForRequest !== id) {
            dispatch(tasksActions.setSelectedTagIdForRequest(id));
        }
    };

    return (
        <span
            onClick = { setSelectedTagIdForRequestHandler }
            className = { selectedTag }
            style = { { color: `${color}`, backgroundColor: `${bg}` } }  >
            { name }
        </span>
    );
};

