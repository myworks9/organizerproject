/* eslint-disable no-nested-ternary */
// core
import { yupResolver } from '@hookform/resolvers/yup';
import { format } from 'date-fns';
import { FC, useEffect } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch, useAppSelector } from '../../../lib/redux/init/reduxTypes';

// types
import {
    ToastOptions, ToDoFormType, ToDoRequestType, UpdateToDoRequestType,
} from '../../../types';

// actions && selectors
import { tasksActions } from '../../../lib/redux/actions/tasksActions';
import {
    isCreateTaskModeSelector,
    isTaskCompleteSelector,
    selectedTagIdForRequestSelector,
    selectedTagIdSelector,
    selectedTaskSelector,
} from '../../../lib/redux/selectors';

// components
import { Input } from '../elements';
import { Head } from './Head';
import { Tags } from './Tags';

// helpers
import { toastConstants, toastMessages } from '../../../constants';
import {
    createToasters, dateFormatter, disablePrevDays, taskCardErrorsCreator,
} from '../../../helpers';
import { schema } from './config';


export const TaskCardForm: FC = () => {
    const dispatch = useAppDispatch();
    const {
        register, handleSubmit, formState, reset,
    } = useForm<ToDoFormType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const isTaskComplete = useAppSelector(isTaskCompleteSelector);
    const selectedTagIdForRequest = useAppSelector(selectedTagIdForRequestSelector);
    const selectedTagId = useAppSelector(selectedTagIdSelector);
    const isCreateTaskMode = useAppSelector(isCreateTaskModeSelector);
    const selectedToDo = useAppSelector(selectedTaskSelector);

    useEffect((): void => {
        reset();
    }, [selectedToDo]);

    const resetFormHandler = (): void => {
        reset();
    };

    const submit = handleSubmit((data: ToDoFormType): void => {
        const newToDoRequestData: ToDoRequestType = {
            ...data,
            completed: false,
            tag:       selectedTagIdForRequest,
        };

        const updateToDoRequestData: UpdateToDoRequestType  = {
            ...data,
            completed: isTaskComplete || false,
            tag:       selectedTagIdForRequest,
            id:        selectedToDo?.id ? selectedToDo.id : '',
        };

        if (isCreateTaskMode) {
            dispatch(tasksActions.setNewToDoRequest(newToDoRequestData));
            createToasters(ToastOptions.success, toastMessages({
                name: toastConstants.ADD_TASK_MESSAGE,
            }));
        }

        if (!isCreateTaskMode) {
            dispatch(tasksActions.updateToDoRequest(updateToDoRequestData));
            dispatch(tasksActions.setSelectedToDo(null));
            createToasters(ToastOptions.success, toastMessages({
                name: toastConstants.CHANGE_TASK_MESSAGE,
            }));
        }

        if (isTaskComplete) {
            dispatch(tasksActions.setToDoComplete(false));
        }

        dispatch(tasksActions.setIsNewTaskFormOpen(false));
    });

    return (
        <form onSubmit = { submit }>
            <Head />
            <div className = 'content'>
                <Input
                    defaultValue = { selectedToDo ? selectedToDo.title : '' }
                    label = 'Задача'
                    register = { register('title') }
                    placeholder = 'Пройти интенсив по React + Redux + TS + Mobx'
                    className = 'title' />
                <div className = 'deadline'>
                    <span className = 'label'>Дедлайн</span>
                    <span className = 'date'>
                        <div className = 'react-datepicker-wrapper'>
                            <Input
                                min = { `${disablePrevDays()}` }
                                defaultValue = {
                                    selectedToDo
                                        ? format(new Date(selectedToDo.deadline), 'yyyy-MM-dd')
                                        : `${dateFormatter()}`
                                }
                                type = 'date'
                                classNameLabel = 'react-datepicker__input-container'
                                className = 'react-datepicker-ignore-onclickoutside'
                                register = { register('deadline') } />
                        </div>
                    </span>
                </div>
                <div className = 'description'>
                    <Input
                        defaultValue = { selectedToDo ? selectedToDo.description : '' }
                        tag = 'textarea'
                        label = 'Описание '
                        classNameLabel = 'label'
                        register = { register('description') }
                        className = 'text'
                        placeholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.' />
                </div>
                <Tags />
                <div className = 'errors'>
                    { formState.errors && taskCardErrorsCreator(formState.errors) }
                </div>
                <div className = 'form-controls'>
                    <button
                        type = 'reset'
                        onClick = { resetFormHandler }
                        className = 'button-reset-task'> Reset
                    </button>
                    <button
                        disabled = {
                            !formState.isDirty && selectedTagIdForRequest === selectedTagId
                        }
                        type = 'submit'
                        className = 'button-save-task'> Save
                    </button>
                </div>
            </div>
        </form>
    );
};
