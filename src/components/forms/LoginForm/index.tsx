// core
import { yupResolver } from '@hookform/resolvers/yup';
import { FC } from 'react';
import { Link } from 'react-router-dom';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch } from '../../../lib/redux/init/reduxTypes';

// actions
import { authActions } from '../../../lib/redux/actions';

// types
import { LoginRequestType } from '../../../types';

// components
import { Input } from '../elements';

// config
import { book } from '../../../book';
import { schema } from './config';

export const LoginForm: FC = () => {
    const dispatch = useAppDispatch();
    const { register, handleSubmit, formState } = useForm<LoginRequestType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const submit = handleSubmit((credentials): void => {
        dispatch(authActions.setRequestToLogin(credentials));
    });

    return (
        <form onSubmit = { submit }>
            <fieldset>
                <legend>Вход</legend>
                <Input
                    placeholder = 'Электропочта'
                    register = { register('email') }
                    error = { formState.errors.email } />
                <Input
                    placeholder = 'Пароль'
                    register = { register('password') }
                    type = 'password'
                    error = { formState.errors.password } />
                <button
                    className = 'button-login'
                    type = 'submit'> Войти
                </button>
            </fieldset>
            <p> Если у вас до сих пор нет учётной записи, вы можете { ' ' }
                <Link to = { book.signUp }>зарегистрироваться</Link>
            </p>
        </form>
    );
};

