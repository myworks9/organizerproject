// core
import * as yup from 'yup';

// types
import { LoginRequestType } from '../../../types';

/* eslint-disable no-template-curly-in-string */
const minLengthError = 'Минимальная длина ${min}';

/* eslint-disable no-template-curly-in-string */
const maxLengthError = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<LoginRequestType> = yup.object().shape({
    email:    yup.string().required('*').email('Поле email обязательно для заполнения'),
    password: yup.string().required('*').min(8, minLengthError).max(12, maxLengthError),
});
