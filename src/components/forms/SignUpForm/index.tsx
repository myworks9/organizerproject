// core
import { yupResolver } from '@hookform/resolvers/yup';
import { FC } from 'react';
import { Link } from 'react-router-dom';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch } from '../../../lib/redux/init/reduxTypes';

// types && actions
import { authActions } from '../../../lib/redux/actions';
import { RegistrationFormType } from '../../../types';

// components
import { Input } from '../elements';

// helpers
import { book } from '../../../book';

// config
import { schema } from './config';

export const SignUpForm: FC = () => {
    const dispatch = useAppDispatch();
    const { register, handleSubmit, formState } = useForm<RegistrationFormType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const submit = handleSubmit((data): void => {
        const { confirmPassword, ...registrationData } = data;
        dispatch(authActions.setRequestToRegistration(registrationData));
    });

    return (
        <form onSubmit = { submit }>
            <fieldset>
                <legend>Регистрация</legend>
                <Input
                    placeholder = 'Имя и фамилия'
                    register = { register('name') }
                    error = { formState.errors.name } />
                <Input
                    placeholder = 'Электропочта'
                    register = { register('email') }
                    error = { formState.errors.email } />
                <Input
                    placeholder = 'Пароль'
                    type = 'password'
                    register = { register('password') }
                    error = { formState.errors.password } />
                <Input
                    placeholder = 'Подтверждение пароля'
                    type = 'password'
                    register = { register('confirmPassword') }
                    error = { formState.errors.confirmPassword } />
                <button
                    className = 'button-login'
                    type = 'submit'> Зарегистрироваться
                </button>
            </fieldset>
            <p> Перейти к { '  ' }
                <Link to = { book.login }> логину </Link>
            </p>
        </form>
    );
};
