// core
import * as yup from 'yup';

// types
import { RegistrationFormType } from '../../../types';

/* eslint-disable no-template-curly-in-string */
const minLengthError = 'Минимальная длина ${min}';

/* eslint-disable no-template-curly-in-string */
const maxLengthError = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<RegistrationFormType> = yup.object().shape({
    name:            yup.string().required('*'),
    email:           yup.string().required('*').email('Поле email обязательно для заполнения'),
    password:        yup.string().required('*').min(8, minLengthError).max(12, maxLengthError),
    confirmPassword: yup.string().required('Повторите пароль').oneOf([yup.ref('password')], 'Пароли не совпадают'),
});
