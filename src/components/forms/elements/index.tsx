/* eslint-disable node/no-missing-import */
/* eslint-disable import/no-unresolved */
import { UseFormRegisterReturn } from 'react-hook-form/dist/types/form';

type InputPropsType = {
    tag?: string;
    type?: string;
    placeholder?: string;
    register: UseFormRegisterReturn;
    label?: string;
    error?: {
        message?: string;
    };
    className?: string
    classNameLabel?: string
    defaultValue?: string
    min?: string
};

export const Input: React.FC<InputPropsType> = (props) => {
    let input = (
        <input
            defaultValue = { props.defaultValue }
            min = { props.min }
            className = { props?.className }
            type = { props.type }
            placeholder = { props.placeholder }
            { ...props.register } />
    );

    if (props.tag === 'textarea') {
        input = (
            <textarea
                className = { props.className }
                placeholder = { props.placeholder }
                defaultValue = { props.defaultValue }
                { ...props.register }></textarea>
        );
    }

    return (
        <label className = { props.classNameLabel }>
            <div>
                { props.label }{ ' ' }
                <span
                    className = 'error-message'
                    style = { { color: 'red', fontSize: '16px' } }> { props.error?.message }
                </span>
            </div>
            { input }
        </label>
    );
};

Input.defaultProps = {
    type:           'text',
    tag:            'input',
    classNameLabel: 'label',
};
