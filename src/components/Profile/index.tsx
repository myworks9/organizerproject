// core
import { FC } from 'react';

// hooks
import { useAppSelector } from '../../lib/redux/init/reduxTypes';

// selectors
import { isFetchingSelector, userProfileDataSelector } from '../../lib/redux/selectors';

// components
import { Spinner } from '../Spinner';

export const Profile: FC = () => {
    const userInfo = useAppSelector(userProfileDataSelector);
    const isFetching = useAppSelector(isFetchingSelector);

    return (
        <h1>{ isFetching ? <Spinner /> : userInfo?.name }</h1>
    );
};
