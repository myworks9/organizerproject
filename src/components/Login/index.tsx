// core
import { FC } from 'react';

// components
import { LoginForm } from '../forms';

export const Login: FC = () => {
    return (
        <section className = 'login-form'>
            <LoginForm />
        </section>
    );
};

