// core
import React from 'react';
import { tasksActions } from '../../../lib/redux/actions/tasksActions';

// hooks
import { useAppDispatch, useAppSelector } from '../../../lib/redux/init/reduxTypes';

// selectors
import {
    isCreateTaskModeSelector,
    isNewTaskFormOpenSelector,
    selectedTaskSelector,
} from '../../../lib/redux/selectors';

export const Controls: React.FC = () => {
    const dispatch = useAppDispatch();
    const selectedToDo = useAppSelector(selectedTaskSelector);
    const isCreateTaskMode = useAppSelector(isCreateTaskModeSelector);
    const isNewTaskFormOpen = useAppSelector(isNewTaskFormOpenSelector);

    const setNewTaskFormOpen = (): void => {
        if (!isCreateTaskMode) {
            dispatch(tasksActions.setIsCreateTaskMode(true));
        }
        if (!isNewTaskFormOpen) {
            dispatch(tasksActions.setIsNewTaskFormOpen(true));
        }
        if (selectedToDo) {
            dispatch(tasksActions.setSelectedToDo(null));
        }
    };

    return (
        <div className = 'controls'>
            <i className = 'las'></i>
            <button
                onClick = { setNewTaskFormOpen }
                className = 'button-create-task'> Новая задача
            </button>
        </div>
    );
};

