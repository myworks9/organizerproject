// core
import { FC } from 'react';

// components
import { TaskCardForm } from '../../../forms';

export const TaskCard: FC = () => {
    return (
        <div className = 'task-card'>
            <TaskCardForm />
        </div>
    );
};

