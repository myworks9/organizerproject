// core
import cx from 'classnames';
import { FC } from 'react';

// hooks
import { useAppSelector } from '../../../../lib/redux/init/reduxTypes';

// selectors
import { tasksSelector } from '../../../../lib/redux/selectors';

// components
import { Task } from './Task';

export const Tasks: FC = () => {
    const tasks = useAppSelector(tasksSelector);

    const tasksJSX = tasks.map((task) => <Task
        task = { task }
        key = { task.id }
        tag = { task.tag }
        title = { task.title }
        deadline = { task.deadline }
        completed = { task.completed } />);

    const isEmptyList = cx('list', {
        empty: !tasks.length,
    });

    return (
        <div className = { isEmptyList }>
            <div className = 'tasks'>
                { tasksJSX }
            </div>
        </div>
    );
};

