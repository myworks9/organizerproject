// core
import cx from 'classnames';
import { FC } from 'react';

// hooks
import _ from 'lodash';
import { useAppDispatch, useAppSelector } from '../../../../../lib/redux/init/reduxTypes';

// types
import { TagsResponseType, ToDoResponseType } from '../../../../../types';

// actions && selectors
import { dateFormatter } from '../../../../../helpers';
import { tasksActions } from '../../../../../lib/redux/actions';
import {
    isCreateTaskModeSelector,
    isNewTaskFormOpenSelector,
    selectedTaskSelector,
} from '../../../../../lib/redux/selectors';

type PropsTypes = {
    task: ToDoResponseType
    title: string,
    completed: boolean
    deadline: string
    tag: TagTypes
};

type TagTypes = Omit<TagsResponseType, 'id'>;

export const Task: FC<PropsTypes> = ({
    title, deadline, tag, completed, task,
}) => {
    const isTaskFormOpen = useAppSelector(isNewTaskFormOpenSelector);
    const selectedTask = useAppSelector(selectedTaskSelector);
    const isCreateTaskMode = useAppSelector(isCreateTaskModeSelector);
    const dispatch = useAppDispatch();

    const completedTask = cx('task', {
        completed,
    });

    const setSelectedTaskHandler = () => {
        if (isCreateTaskMode) {
            dispatch(tasksActions.setIsCreateTaskMode(false));
        }
        if (!_.isEqual(selectedTask, task)) {
            dispatch(tasksActions.setSelectedToDo(task));
        }
        if (!isTaskFormOpen) {
            dispatch(tasksActions.setIsNewTaskFormOpen(true));
        }
    };

    return (
        <div
            onClick = { setSelectedTaskHandler }
            className = { completedTask }>

            <span className = 'title'>{ title }</span>
            <div className = 'meta'>
                <span className = 'deadline'>{ dateFormatter(deadline) }</span>
                <span className = 'tag' style = { { color: `${tag?.color}`, backgroundColor: `${tag?.bg}` } }  >
                    { tag?.name }
                </span>
            </div>
        </div>
    );
};

