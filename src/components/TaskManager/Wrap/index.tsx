// core
import { FC } from 'react';

// types
import { isNewTaskFormOpenSelector } from '../../../lib/redux/selectors';

// selectors
import { useAppSelector } from '../../../lib/redux/init/reduxTypes';

// components
import { TaskCard } from './TaskCard';
import { Tasks } from './Tasks';

export const Wrap: FC = () => {
    const isNewTaskFormOpen = useAppSelector(isNewTaskFormOpenSelector);

    return (
        <div className = 'wrap'>
            <Tasks />
            { isNewTaskFormOpen && <TaskCard /> }
        </div>
    );
};

