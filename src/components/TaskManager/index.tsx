// core
import { FC } from 'react';
import { Navigate } from 'react-router-dom';

// hooks
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/reduxTypes';

// types
import { status, ToastOptions } from '../../types';

// selectors
import { isFetchingSelector, tokenSelector } from '../../lib/redux/selectors';

// helpers
import { book } from '../../book';
import { toastConstants, toastMessages } from '../../constants';
import { changeAuthorizationStatus, createToasters } from '../../helpers';

// components
import { Spinner } from '../Spinner';
import { Controls } from './Controls';
import { Wrap } from './Wrap';

export const TaskManager: FC = () => {
    const token = localStorage.getItem('token');

    const storageToken = useAppSelector(tokenSelector);
    const isFetching = useAppSelector(isFetchingSelector);
    const dispatch = useAppDispatch();

    if (!token && !storageToken) {
        return <Navigate to = { book.login } />;
    }
    if (!storageToken && token) {
        changeAuthorizationStatus({
            token, status: status.SET_AUTHORIZED, dispatch,
        });
        createToasters(
            ToastOptions.success,
            toastMessages({
                name: toastConstants.SUCCESS_AUTHORIZATION,
            }),
        );
    }

    return (
        isFetching ? <Spinner /> : (
            <>
                <Controls />
                <Wrap />
            </>)
    );
};
