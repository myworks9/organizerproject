// core
import { AxiosResponse } from 'axios';
import { ToDoRequestType, ToDoResponseType } from '../types';
import { UpdateToDoRequestType } from '../types/API/todo';
import { rootAPI } from './index';

export const todoAPI = {
    async getAllTodo(): Promise<ToDoResponseType[] | null> {
        const token = localStorage.getItem('token');
        if (!token) {
            return null;
        }
        const response = await rootAPI.get<AxiosResponse<ToDoResponseType[]>>('/tasks', {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return response?.data?.data;
    },
    async createToDo(todo: ToDoRequestType): Promise<ToDoResponseType> {
        const response = await rootAPI.post<AxiosResponse<ToDoResponseType, ToDoRequestType>>('/tasks', todo);

        return response?.data?.data;
    },
    async updateToDo(todo: UpdateToDoRequestType): Promise<ToDoResponseType> {
        const { id, ...todoRequest } = todo;
        const response = await rootAPI.put<AxiosResponse<ToDoResponseType, ToDoRequestType>>(`/tasks/${id}`, todoRequest);

        return response?.data?.data;
    },
    async deleteToDo(id: string): Promise<AxiosResponse> {
        const response = await rootAPI.delete<AxiosResponse<void, string>>(`/tasks/${id}`);

        return response;
    },
} as const;
