// core
import { AxiosResponse } from 'axios';
import {
    AuthResponseType, LoginRequestType, ProfileResponseType, RegistrationRequestType,
} from '../types';
import { rootAPI } from './index';

export const authAPI = {
    async getUserProfile(): Promise<ProfileResponseType> {
        const response = await rootAPI.get<ProfileResponseType>('/auth/profile');

        return response?.data;
    },
    async signUp(credentials: RegistrationRequestType): Promise<AuthResponseType> {
        const response = await rootAPI.post<AxiosResponse<AuthResponseType, RegistrationRequestType>>('/auth/registration', credentials);

        return response?.data?.data;
    },
    async login(credentials: LoginRequestType): Promise<AuthResponseType> {
        const { email, password } = credentials;
        const response = await rootAPI.get<AxiosResponse<AuthResponseType, LoginRequestType>>('/auth/login', {
            headers: {
                Authorization: `Basic ${window.btoa(`${email}:${password}`)}`,
            },
        });

        return response?.data?.data;
    },
    async logOut(): Promise<void> {
        await rootAPI.get<AxiosResponse<void>>('/auth/logout');
    },
} as const;
