// core
import axios from 'axios';

// api
import { authAPI } from './authAPI';
import { tagsAPI } from './tagsAPI';
import { todoAPI } from './todoAPI';

// instance
export const rootAPI = axios.create({
    baseURL: 'https://lab.lectrum.io/rtx/api/v2/todos',
});

export const api = {
    todo: {
        ...todoAPI,
    },
    auth: {
        ...authAPI,
    },
    tags: {
        ...tagsAPI,
    },
} as const;
