// core
import { TagsResponseType } from '../types';
import { rootAPI } from './index';

export const tagsAPI = {
    async getAllTags(): Promise<TagsResponseType[]> {
        const response = await rootAPI.get<TagsResponseType[]>('/tags');

        return response?.data;
    },
} as const;
