// core
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export const dateFormatter = (date: string | Date = new Date()): string =>  {
    return format(new Date(date), typeof date === 'string' ? 'dd/MMM/yyyy' : 'yyyy-MM-dd', { locale: ru });
};
