export const disablePrevDays = (): string => {
    const dtToday = new Date();

    let month: string | number = dtToday.getMonth() + 1;
    let day: string | number = dtToday.getDate();
    const year = dtToday.getFullYear();
    if (month < 10) { month = `0${month.toString()}`; }
    if (day < 10) { day = `0${day.toString()}`; }

    const maxDate = `${year}-${month}-${day}`;

    return maxDate;
};
