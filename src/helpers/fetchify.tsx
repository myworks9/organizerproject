// core
import { ReactElement } from 'react';

type JSXType = null | undefined | string | ReactElement[];

type fetchifyReturn = ReactElement[] | null | string | ReactElement;

export const fetchify = (isFetching: boolean, JSX: JSXType): fetchifyReturn => {
    if (isFetching) {
        return (
            <span style = { { color: '#000', fontSize: '14px' } }>
                Data is loading...
            </span>
        );
    }

    if (typeof JSX !== 'undefined') {
        return JSX;
    }

    return null;
};

