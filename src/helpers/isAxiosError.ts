/* eslint-disable @typescript-eslint/no-explicit-any */
// core
import { AxiosError } from 'axios';

// types
type AxiosErrorResponseType = {
    statusCode: number;
    message: string;
    error: string;
};

export const isAxiosError = (error: any): error is AxiosError<AxiosErrorResponseType> => {
    return error.isAxiosError === true;
};
