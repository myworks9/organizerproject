// core
import { rootAPI } from '../api/index';

// types
import { AppDispatch } from '../lib/redux/init/reduxTypes';
import { status } from '../types';

// actions
import { authActions } from '../lib/redux/actions/authActions';

type ArgTypes = {
    status: status,
    token: string | null
    dispatch: AppDispatch,
};

type ChangeAuthorizationStatusType = (args: ArgTypes) => null | undefined;

export const changeAuthorizationStatus: ChangeAuthorizationStatusType = (args) => {
    if (args.status === status.SET_AUTHORIZED) {
        localStorage.setItem('token', typeof args.token === 'string' ? args.token : '');
        args.dispatch(authActions.setToken(args.token));
        rootAPI.defaults.headers.common.Authorization = `Bearer ${args.token}`;

        return null;
    }
    localStorage.removeItem('token');
    args.dispatch(authActions.setToken(args.token));
    delete rootAPI.defaults.headers.common.Authorization;
};
