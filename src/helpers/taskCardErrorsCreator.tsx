/* eslint-disable node/no-missing-import */
/* eslint-disable import/no-unresolved */
import { ReactElement } from 'react';
import { FieldErrors } from 'react-hook-form/dist/types';
import { ToDoFormType } from '../types';

export const taskCardErrorsCreator = (
    errors: FieldErrors<ToDoFormType>,
): ReactElement | undefined => {
    if (errors.title && errors.description) {
        return (
            <>
                <p className = 'errorMessage'>{ errors.description?.message }</p>
                <p className = 'errorMessage'>{ errors.title?.message }</p>
            </>
        );
    }
    if (errors.title && !errors.description) {
        return <p className = 'errorMessage'>{ errors.title?.message }</p>;
    }
    if (errors.description && !errors.title) {
        return <p className = 'errorMessage'>{ errors.description?.message }</p>;
    }
};
