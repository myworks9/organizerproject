// core
import { Id, toast } from 'react-toastify';

// types
import { ToastOptions } from '../types';

// constants
import { toastOptions } from '../constants';

export type CreateToastersType = <O extends ToastOptions>(
    typeOfToasts: O,
    message: string
) => Id;

export const createToasters: CreateToastersType = (typeOfToast, message) => {
    return toast[ typeOfToast ](message, toastOptions);
};

