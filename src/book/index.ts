// types
type BookTypes = {
    '*': string
    '/': string
    taskManager: string
    profile: string
    login: string
    signUp: string
};

export const book: BookTypes = {
    '/':         '/',
    '*':         '*',
    taskManager: '/todo/task-manager',
    profile:     '/todo/profile',
    login:       '/todo/login',
    signUp:      '/todo/sign-up',
} as const;
