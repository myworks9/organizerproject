/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable node/no-unpublished-import */
/* eslint-disable node/no-missing-import */
/* eslint-disable import/no-unresolved */
// core
import { CombinatorEffect } from '@redux-saga/types';
import {
    all, fork, ForkEffect,
} from 'redux-saga/effects';

// sagas
import { authWatcher } from './auth';
import { tagsWatcher } from './tags';
import { todoWatcher } from './todo';

type EffectsTypes =
  Generator<ForkEffect<never>, void, unknown> | CombinatorEffect<'ALL', ForkEffect<any>>;

export function* rootSaga(): Generator<EffectsTypes> {
    yield all([fork(authWatcher), fork(tagsWatcher), fork(todoWatcher)]);
}
