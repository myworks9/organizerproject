// core
import {
    call, CallEffect, put, PutEffect, select, SelectEffectDescriptor, SimpleEffect, takeLatest,
} from 'redux-saga/effects';

// types
import {
    SetErrorActionType, SetIsFetchingActionType, SetTagsActionType, TagsResponseType,
} from '../../../types';
import { tasksTypes } from '../types';

// actions && selectors
import { tasksActions, uiActions } from '../actions';
import { tagsSelector } from '../selectors';

// api
import { api } from '../../../api';

// helpers
import { isAxiosError } from '../../../helpers';

type EffectsTypes = CallEffect<TagsResponseType[]>
| PutEffect<SetIsFetchingActionType | SetTagsActionType | SetErrorActionType>
| SimpleEffect<'SELECT', SelectEffectDescriptor>;

function* tagsWorker(
    action: SetTagsActionType,
): Generator<EffectsTypes, null | unknown, TagsResponseType[]> {
    const tags = yield select(tagsSelector);

    if (!action.payload || tags.length > 0) {
        return null;
    }
    try {
        yield put(uiActions.setIsFetching(true));
        const response: TagsResponseType[] = yield call(api.tags.getAllTags);
        if (response.length) {
            yield put(tasksActions.setTags(response));
        }
    } catch (error) {
        if (isAxiosError(error)) {
            yield put(uiActions.setError(error.response?.data.message));
        }
    } finally {
        yield put(uiActions.setIsFetching(false));
    }
}


export function* tagsWatcher() {
    yield takeLatest(tasksTypes.SET_IS_NEW_TASK_FORM_OPEN, tagsWorker);
}
