// core
import { push } from 'redux-first-history';
import { CallHistoryMethodAction, LOCATION_CHANGE } from 'redux-first-history/build/es6/actions';
import {
    call,
    CallEffect,
    ForkEffect,
    put,
    PutEffect,
    select,
    SelectEffectDescriptor,
    SimpleEffect,
    takeLatest,
} from 'redux-saga/effects';
import { SetRequestToLoginActionType, SetUserProfileDataActionType } from '../../../types/actions/auth';
import { userProfileDataSelector } from '../selectors/authSelectors';

// api
import { api } from '../../../api';

// actions
import { authActions, uiActions } from '../actions';

// types
import {
    AuthResponseType,
    LocationActionType,
    ProfileResponseType,
    SetErrorActionType,
    SetIsFetchingActionType,
    SetRequestToRegistrationActionType,
    SetTokenActionType,
    status,
} from '../../../types';
import { authTypes } from '../types';

// helpers
import { book } from '../../../book';
import { changeAuthorizationStatus, isAxiosError } from '../../../helpers';

type EffectsTypes =
    | CallEffect<AuthResponseType | ProfileResponseType>
    | PutEffect<SetIsFetchingActionType
    | SetUserProfileDataActionType
    | SetTokenActionType
    | CallHistoryMethodAction
    | SetErrorActionType>
    | SimpleEffect<'SELECT', SelectEffectDescriptor>;

function* registrationWorker(
    action: SetRequestToRegistrationActionType,
): Generator<EffectsTypes, void, string> {
    try {
        yield put(uiActions.setIsFetching(true));
        const response = yield call(
            api.auth.signUp, action.payload,
        );

        if (response.length) {
            changeAuthorizationStatus({
                status: status.SET_AUTHORIZED, dispatch: put, token: response,
            });
            yield put(push(book.taskManager));
        }
    } catch (error) {
        if (isAxiosError(error)) {
            yield put(uiActions.setError(error.response?.data.message));
        }
    } finally {
        yield put(uiActions.setIsFetching(false));
    }
}

function* loginWorker(
    action: SetRequestToLoginActionType,
): Generator<EffectsTypes, void, string>  {
    try {
        yield put(uiActions.setIsFetching(true));
        const response = yield call(
            api.auth.login, action.payload,
        );

        if (response.length) {
            changeAuthorizationStatus({
                status: status.SET_AUTHORIZED, dispatch: put, token: response,
            });
            yield put(push(book.taskManager));
        }
    } catch (error) {
        if (isAxiosError(error)) {
            yield put(uiActions.setError(error.response?.data.message));
        }
    }  finally {
        yield put(uiActions.setIsFetching(false));
    }
}

function* profileWorker(
    action: LocationActionType,
): Generator<EffectsTypes, void, ProfileResponseType>  {
    const profileData = yield select(userProfileDataSelector);
    if (action.payload.location.pathname.includes(book.profile) && !profileData) {
        try {
            yield put(uiActions.setIsFetching(true));
            const response: ProfileResponseType = yield call(
                api.auth.getUserProfile,
            );

            if (response) {
                yield put(authActions.setUserProfileData(response));
            }
        } catch (error) {
            if (isAxiosError(error)) {
                if (error.response?.status === 401 || error.response?.status === 404) {
                    changeAuthorizationStatus({
                        status: status.SET_UNAUTHORIZED, dispatch: put, token: null,
                    });
                    yield put(push(book.login));
                }
                yield put(uiActions.setError(error.response?.data.message));
            }
        }  finally {
            yield put(uiActions.setIsFetching(false));
        }
    }
}

export function* authWatcher(): Generator<ForkEffect<never>> {
    yield takeLatest(authTypes.SET_REQUEST_TO_REGISTRATION, registrationWorker);
    yield takeLatest(authTypes.SET_REQUEST_TO_LOGIN, loginWorker);
    yield takeLatest(LOCATION_CHANGE, profileWorker);
}
