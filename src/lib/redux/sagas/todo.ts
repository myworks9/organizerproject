// core
import { AxiosResponse } from 'axios';
import { LOCATION_CHANGE, push } from 'redux-first-history/build/es6/actions';
import {
    call,
    CallEffect,
    put,
    PutEffect,
    PutEffectDescriptor,
    select,
    SelectEffectDescriptor,
    SimpleEffect,
    takeLatest,
} from 'redux-saga/effects';

// types
import {
    DeleteToDoActionType,
    DeleteToDoRequestActionType,
    GetAllTODOSActionType,
    LocationActionType,
    SetErrorActionType,
    SetIsFetchingActionType,
    SetNewToDoActionType,
    SetNewToDoRequestActionType,
    status,
    ToDoResponseType,
    UpdateToDoActionType,
    UpdateToDoRequestActionType,
} from '../../../types';
import { tasksTypes } from '../types';

// actions && selectors
import { tasksActions, uiActions } from '../actions';
import { selectedTaskSelector, tasksSelector } from '../selectors';

// helpers
import { book } from '../../../book';
import { changeAuthorizationStatus, isAxiosError } from '../../../helpers';

// core
import { api } from '../../../api';


type EffectsTypes = CallEffect<ToDoResponseType
| ToDoResponseType[]
| null
| AxiosResponse>
| SimpleEffect<'PUT', PutEffectDescriptor<UpdateToDoActionType>>
| PutEffect<SetIsFetchingActionType
| DeleteToDoActionType
| SetNewToDoActionType
| SetErrorActionType
| GetAllTODOSActionType>
| SimpleEffect<'SELECT', SelectEffectDescriptor>;

function* setNewToDoWorker(
    action: SetNewToDoRequestActionType,
): Generator<EffectsTypes, null | unknown, ToDoResponseType> {
    if (!action.payload) {
        return null;
    }
    try {
        yield put(uiActions.setIsFetching(true));
        const response: ToDoResponseType = yield call(api.todo.createToDo, action.payload);
        if (response) {
            yield put(tasksActions.setNewToDo(response));
        }
    } catch (error) {
        if (isAxiosError(error)) {
            yield put(uiActions.setError(error.response?.data.message));
        }
    } finally {
        yield put(uiActions.setIsFetching(false));
    }
}

function* getAllTODOS(
    action: LocationActionType,
): Generator<EffectsTypes, void, ToDoResponseType[] | []> {
    const tasks = yield select(tasksSelector);
    if (action.payload.location.pathname.includes(book.taskManager) && tasks.length === 0) {
        try {
            yield put(uiActions.setIsFetching(true));
            const response: ToDoResponseType[] = yield call(api.todo.getAllTodo);
            if (response) {
                yield put(tasksActions.getAllTODOS(response));
            }
        } catch (error) {
            if (isAxiosError(error)) {
                if (error.response?.status === 401 || error.response?.status === 404) {
                    changeAuthorizationStatus({
                        status: status.SET_UNAUTHORIZED, dispatch: put, token: null,
                    });
                    put(push(book.login));
                }
                yield put(uiActions.setError(error.response?.data.message));
            }
        } finally {
            yield put(uiActions.setIsFetching(false));
        }
    }
}

function* updateToDoById(
    action: UpdateToDoRequestActionType,
): Generator<EffectsTypes, void, ToDoResponseType> {
    try {
        yield put(uiActions.setIsFetching(true));
        const response: ToDoResponseType = yield call(api.todo.updateToDo, action.payload);
        if (response) {
            yield put(tasksActions.updateToDo(response));
        }
    } catch (error) {
        if (isAxiosError(error)) {
            if (error.response?.status === 401 || error.response?.status === 404) {
                changeAuthorizationStatus({
                    status: status.SET_UNAUTHORIZED, dispatch: put, token: null,
                });
                put(push(book.login));
            }
            yield put(uiActions.setError(error.response?.data.message));
        }
    } finally {
        yield put(uiActions.setIsFetching(false));
    }
}

function* deleteToDo(
    action: DeleteToDoRequestActionType,
): Generator<EffectsTypes, void, AxiosResponse & ToDoResponseType> {
    const selectedTask = yield select(selectedTaskSelector);

    try {
        yield put(uiActions.setIsFetching(true));
        const response: AxiosResponse = yield call(
            api.todo.deleteToDo, action.payload,
        );
        if (response.status === 204) {
            yield put(tasksActions.deleteToDo(selectedTask.id));
        }
    } catch (error) {
        if (isAxiosError(error)) {
            if (error.response?.status === 401 || error.response?.status === 404) {
                changeAuthorizationStatus({
                    status: status.SET_UNAUTHORIZED, dispatch: put, token: null,
                });
                put(push(book.login));
            }
            yield put(uiActions.setError(error.response?.data.message));
        }
    } finally {
        yield put(uiActions.setIsFetching(false));
    }
}

export function* todoWatcher() {
    yield takeLatest(tasksTypes.SET_NEW_TODO_REQUEST, setNewToDoWorker);
    yield takeLatest(LOCATION_CHANGE, getAllTODOS);
    yield takeLatest(tasksTypes.UPDATE_TODO_REQUEST, updateToDoById);
    yield takeLatest(tasksTypes.DELETE_TODO_REQUEST, deleteToDo);
}
