// types
import {
    DeleteToDoActionType,
    DeleteToDoRequestActionType,
    GetAllTODOSActionType,
    SetIsCreateTaskModeActionType,
    SetIsNewTaskFormOpenActionType,
    SetNewToDoActionType,
    SetNewToDoRequestActionType,
    SetSelectedTagIdForRequestActionType,
    SetSelectedToDoActionType,
    SetTagsActionType,
    SetToDoCompleteActionType,
    TagsResponseType,
    ToDoRequestType,
    ToDoResponseType,
    UpdateToDoActionType,
    UpdateToDoRequestActionType,
    UpdateToDoRequestType,
} from '../../../types';
import { tasksTypes } from '../types';

type tasksActionsTypes = {
    setIsNewTaskFormOpen: (payload: boolean) => SetIsNewTaskFormOpenActionType
    setTags: (payload: TagsResponseType[]) => SetTagsActionType
    setSelectedTagIdForRequest: (payload: string) => SetSelectedTagIdForRequestActionType
    setNewToDoRequest: (payload: ToDoRequestType) => SetNewToDoRequestActionType
    setNewToDo: (payload: ToDoResponseType) => SetNewToDoActionType
    getAllTODOS: (payload: ToDoResponseType[]) => GetAllTODOSActionType
    setSelectedToDo: (payload: ToDoResponseType | null) => SetSelectedToDoActionType
    setIsCreateTaskMode: (payload: boolean) => SetIsCreateTaskModeActionType
    updateToDoRequest: (payload: UpdateToDoRequestType) => UpdateToDoRequestActionType
    updateToDo: (payload: ToDoResponseType) => UpdateToDoActionType
    setToDoComplete: (payload: boolean) => SetToDoCompleteActionType
    deleteToDoRequest: (payload: string) => DeleteToDoRequestActionType
    deleteToDo: (payload: string) => DeleteToDoActionType
};

export const tasksActions: tasksActionsTypes = {
    setIsNewTaskFormOpen: (
        payload,
    ) => ({ type: tasksTypes.SET_IS_NEW_TASK_FORM_OPEN, payload }),
    setTags:                    (payload) => ({ type: tasksTypes.SET_TAGS, payload }),
    setSelectedTagIdForRequest: (
        payload,
    ) => ({ type: tasksTypes.SET_SELECTED_TAG_ID_FOR_REQUEST, payload }),
    setNewToDoRequest:   (payload) => ({ type: tasksTypes.SET_NEW_TODO_REQUEST, payload }),
    setNewToDo:          (payload) => ({ type: tasksTypes.SET_NEW_TODO, payload }),
    getAllTODOS:         (payload) => ({ type: tasksTypes.GET_ALL_TODOS, payload }),
    setSelectedToDo:     (payload) => ({ type: tasksTypes.SET_SELECTED_TODO, payload }),
    setIsCreateTaskMode: (payload) => ({ type: tasksTypes.SET_IS_CREATE_TASK_MODE, payload }),
    updateToDoRequest:   (payload) => ({ type: tasksTypes.UPDATE_TODO_REQUEST, payload }),
    updateToDo:          (payload) => ({ type: tasksTypes.UPDATE_TODO, payload }),
    setToDoComplete:     (payload) => ({ type: tasksTypes.SET_TODO_COMPLETE, payload }),
    deleteToDoRequest:   (payload) => ({ type: tasksTypes.DELETE_TODO_REQUEST, payload }),
    deleteToDo:          (payload) => ({ type: tasksTypes.DELETE_TODO, payload }),
} as const;
