// types
import { SetErrorActionType, SetIsFetchingActionType } from '../../../types';
import { uiTypes } from '../types';

type uiActionsTypes = {
    setIsFetching: (payload: boolean) => SetIsFetchingActionType
    setError: (payload: string | null | undefined) => SetErrorActionType
};

export const uiActions: uiActionsTypes = {
    setIsFetching: (payload) => ({ type: uiTypes.SET_IS_FETCHING, payload }),
    setError:      (payload) => ({ type: uiTypes.SET_ERROR_MESSAGE, payload }),
} as const;
