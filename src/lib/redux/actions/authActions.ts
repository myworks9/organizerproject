// types
import {
    LoginRequestType,
    ProfileResponseType,
    RegistrationRequestType,
    SetRequestToLoginActionType,
    SetRequestToRegistrationActionType,
    SetTokenActionType,
    SetUserProfileDataActionType,
} from '../../../types';
import { authTypes } from '../types';

export type AuthActionsTypes = {
    setRequestToRegistration: (payload: RegistrationRequestType)
    => SetRequestToRegistrationActionType
    setRequestToLogin: (payload: LoginRequestType)
    => SetRequestToLoginActionType
    setToken: (payload: string | null) => SetTokenActionType
    setUserProfileData: (payload: ProfileResponseType | null) => SetUserProfileDataActionType
};

export const authActions: AuthActionsTypes = {
    setRequestToRegistration: (payload) => ({
        type: authTypes.SET_REQUEST_TO_REGISTRATION,
        payload,
    }),
    setRequestToLogin: (payload) => ({
        type: authTypes.SET_REQUEST_TO_LOGIN,
        payload,
    }),
    setToken: (payload) => ({
        type: authTypes.SET_TOKEN,
        payload,
    }),
    setUserProfileData: (payload) => ({ type: authTypes.SET_USER_PROFILE_DATA, payload }),
} as const;
