// types
import { SetErrorActionType, SetIsFetchingActionType } from '../../../types';
import { uiTypes } from '../types';

type InitialStateType = {
    isFetching: boolean,
    errorMessage: null | string | undefined,
};

const initialState: InitialStateType = {
    isFetching:   false,
    errorMessage: null,
};

type UiReducerActionsTypes = SetErrorActionType | SetIsFetchingActionType;

export const uiReducer = (
    state = initialState,
    action: UiReducerActionsTypes,
): InitialStateType => {
    switch (action.type) {
        case uiTypes.SET_IS_FETCHING:
            return {
                ...state,
                isFetching: action.payload,
            };
        case uiTypes.SET_ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: action.payload,
            };
        default: return state;
    }
};
