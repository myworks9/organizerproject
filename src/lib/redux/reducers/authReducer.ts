// types
import {
    ProfileResponseType,
    SetRequestToLoginActionType,
    SetRequestToRegistrationActionType,
    SetTokenActionType,
    SetUserProfileDataActionType,
} from '../../../types';
import { authTypes } from '../types';

type InitialStateType = {
    isUserAuthorized: boolean
    token: string | null
    userData: ProfileResponseType | null
};

const initialState: InitialStateType = {
    isUserAuthorized: false,
    token:            null,
    userData:         null,
};

type AuthReducerActionsTypes =
    | SetRequestToRegistrationActionType
    | SetTokenActionType
    | SetRequestToLoginActionType
    | SetUserProfileDataActionType;

export const authReducer = (
    state = initialState,
    action: AuthReducerActionsTypes,
): InitialStateType => {
    switch (action.type) {
        case authTypes.SET_TOKEN:
            return {
                ...state,
                token: action.payload,
            };
        case authTypes.SET_USER_PROFILE_DATA:
            return {
                ...state,
                userData: action.payload,
            };
        default: return state;
    }
};
