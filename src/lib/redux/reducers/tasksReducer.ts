/* eslint-disable no-debugger */
/* eslint-disable no-confusing-arrow */
// types
import {
    DeleteToDoActionType,
    DeleteToDoRequestActionType,
    GetAllTODOSActionType,
    SetIsCreateTaskModeActionType,
    SetIsNewTaskFormOpenActionType,
    SetNewToDoActionType,
    SetNewToDoRequestActionType,
    SetSelectedTagIdForRequestActionType,
    SetSelectedToDoActionType,
    SetTagsActionType, SetToDoCompleteActionType, TagsResponseType,
    ToDoResponseType,
    UpdateToDoActionType,
    UpdateToDoRequestActionType,
} from '../../../types';
import { tasksTypes } from '../types';

type InitialStateType = {
    isNewTaskFormOpen: boolean
    tags: [] | TagsResponseType[]
    tasks: [] | ToDoResponseType[]
    selectedTask: null | ToDoResponseType
    selectedTagId: string
    selectedTagIdForRequest: string
    isCreateTaskMode: boolean
    isTaskComplete: boolean
};

const initialState: InitialStateType = {
    isTaskComplete:          false,
    isCreateTaskMode:        false,
    selectedTask:            null,
    isNewTaskFormOpen:       false,
    tasks:                   [],
    tags:                    [],
    selectedTagId:           '',
    selectedTagIdForRequest: '',
};

type TasksReducerActionsTypes =
    SetIsNewTaskFormOpenActionType
    | SetTagsActionType
    | SetNewToDoRequestActionType
    | SetNewToDoActionType
    | GetAllTODOSActionType
    | SetSelectedToDoActionType
    | SetIsCreateTaskModeActionType
    | UpdateToDoRequestActionType
    | UpdateToDoActionType
    | SetSelectedTagIdForRequestActionType
    | SetToDoCompleteActionType
    | DeleteToDoRequestActionType
    | DeleteToDoActionType;

export const tasksReducer = (
    state = initialState,
    action: TasksReducerActionsTypes,
): InitialStateType => {
    switch (action.type) {
        case tasksTypes.SET_IS_NEW_TASK_FORM_OPEN:
            return {
                ...state,
                isNewTaskFormOpen: action.payload,
            };
        case tasksTypes.SET_TAGS:
            return {
                ...state,
                tags:                    [...action.payload],
                selectedTagIdForRequest: state.selectedTagId || action.payload[ 0 ]?.id,
            };
        case tasksTypes.SET_SELECTED_TAG_ID_FOR_REQUEST:
            return {
                ...state,
                selectedTagIdForRequest: action.payload,
            };
        case tasksTypes.SET_NEW_TODO:
            return {
                ...state,
                tasks: [action.payload, ...state.tasks],
            };
        case tasksTypes.GET_ALL_TODOS:
            return {
                ...state,
                tasks: [...action.payload],
            };
        case tasksTypes.SET_SELECTED_TODO:
            return {
                ...state,
                selectedTask:  action.payload,
                selectedTagId: action.payload
                    ? action.payload?.tag?.id
                    : state.tags[ 0 ]?.id,
                selectedTagIdForRequest: action.payload
                    ? action.payload?.tag?.id
                    : state.tags[ 0 ]?.id,
            };
        case tasksTypes.SET_IS_CREATE_TASK_MODE:
            return {
                ...state,
                isCreateTaskMode: action.payload,
            };
        case tasksTypes.UPDATE_TODO:
            return {
                ...state,
                tasks: [
                    ...state.tasks.map((task) => {
                        if (task.id === action.payload.id) {
                            return action.payload;
                        }

                        return task;
                    }),
                ],
            };
        case tasksTypes.SET_TODO_COMPLETE:
            return {
                ...state,
                isTaskComplete: action.payload,
            };
        case tasksTypes.DELETE_TODO:
            return {
                ...state,
                tasks: [...state.tasks.filter((task) => task.id !== action.payload)],
            };
        default: return state;
    }
};
