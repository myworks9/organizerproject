// Core
import { applyMiddleware, createStore } from 'redux';
import { rootSaga } from '../sagas';
import { createReduxHistory } from './reduxHistoryContext';
import { rootReducer } from './rootReducer';

// Instruments
import {
    composeEnhancers,
    middleware,
    sagaMiddleWare,
} from './middleware';

export const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middleware)),
);

sagaMiddleWare.run(rootSaga);

export const history = createReduxHistory(store);
