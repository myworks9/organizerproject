// Core
import { AnyAction, compose, Middleware } from 'redux';
/* eslint-disable import/no-extraneous-dependencies, node/no-unpublished-import */
import createSagaMiddleware from '@redux-saga/core';
import { createLogger } from 'redux-logger';
import { routerMiddleware } from './reduxHistoryContext';

export const logger = createLogger({
    duration:  true,
    collapsed: true,
    colors:    {
        title: (action: AnyAction) => {
            return action.error ? 'firebrick' : 'deepskyblue';
        },
        prevState: (): string => '#1C5FAF',
        action:    (): string => '#149945',
        nextState: (): string => '#A47104',
        error:     (): string => '#ff0005',
    },
});

const developmentEnvironment = process.env.NODE_ENV === 'development';
const devtools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const composeEnhancers = developmentEnvironment && devtools ? devtools : compose;

export const sagaMiddleWare = createSagaMiddleware();

const middleware: Middleware[] = [sagaMiddleWare, routerMiddleware];

if (developmentEnvironment) {
    middleware.push(logger);
}

export { composeEnhancers, middleware };

