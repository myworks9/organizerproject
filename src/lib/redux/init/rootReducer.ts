// Core
import { combineReducers } from 'redux';

// Reducers
import {
    authReducer as auth, tasksReducer as tasks, uiReducer as ui,
} from '../reducers';
import { routerReducer as router } from './reduxHistoryContext';

export const rootReducer = combineReducers({
    tasks,
    auth,
    ui,
    router,
});
