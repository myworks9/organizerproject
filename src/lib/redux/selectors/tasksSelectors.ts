// core
import { RootState } from '../init/reduxTypes';

export const isNewTaskFormOpenSelector = (state: RootState) => state.tasks.isNewTaskFormOpen;
export const tasksSelector = (state: RootState) => state.tasks.tasks;
export const selectedTaskSelector = (state: RootState) => state.tasks.selectedTask;
export const tagsSelector = (state: RootState) => state.tasks.tags;
export const selectedTagIdSelector = (state: RootState) => state.tasks.selectedTagId;
export const selectedTagIdForRequestSelector = (
    state: RootState,
) => state.tasks.selectedTagIdForRequest;
export const isCreateTaskModeSelector = (state: RootState) => state.tasks.isCreateTaskMode;
export const isTaskCompleteSelector = (state: RootState) => state.tasks.isTaskComplete;
