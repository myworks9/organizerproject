// core
import { RootState } from '../init/reduxTypes';

export const isFetchingSelector = (state: RootState) => state.ui.isFetching;
export const errorMessageSelector = (state: RootState) => state.ui.errorMessage;
