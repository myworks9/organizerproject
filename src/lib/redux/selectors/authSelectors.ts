// core
import { RootState } from '../init/reduxTypes';

export const tokenSelector = (state:RootState) => state.auth.token;
export const userProfileDataSelector = (state:RootState) => state.auth.userData;
