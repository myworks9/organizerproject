export const enum uiTypes {
    SET_IS_FETCHING = 'SET_IS_FETCHING',
    SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE',
}
