// type
export type ToastConstantsType = {
    QUIT_MESSAGE: string
    ADD_TASK_MESSAGE: string
    DELETE_TASK_MESSAGE: string
    CHANGE_TASK_MESSAGE: string
    SUCCESS_AUTHORIZATION: string
};

export const toastConstants: ToastConstantsType = {
    ADD_TASK_MESSAGE:      'ADD_TASK_MESSAGE',
    QUIT_MESSAGE:          'QUIT_MESSAGE',
    SUCCESS_AUTHORIZATION: 'SUCCESS_AUTHORIZATION',
    DELETE_TASK_MESSAGE:   'DELETE_TASK_MESSAGE',
    CHANGE_TASK_MESSAGE:   'CHANGE_TASK_MESSAGE',
} as const;

