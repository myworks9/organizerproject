// constants
import { toastConstants } from './toastConstants';

type ToastMessagesType = {
    name: string;
    payload?: string;
};

type ToastMessageType = (message: ToastMessagesType) => string;

export const toastMessages: ToastMessageType = (message) => {
    switch (message.name) {
        case toastConstants.SUCCESS_AUTHORIZATION:
            return 'Добро пожаловать.';
        case toastConstants.QUIT_MESSAGE:
            return 'Возвращайтесь еще! Не забывайте о строках выполнения задач!)';
        case toastConstants.ADD_TASK_MESSAGE:
            return 'Поздравляем, задача успешно добавлена!!';
        case toastConstants.CHANGE_TASK_MESSAGE:
            return 'Задача успешно обновлена!!';
        case toastConstants.DELETE_TASK_MESSAGE:
            return 'Задача успешно удалена!!';
        default:
            return 'Не предусмотренное сообщение, но все равно поздравляем!';
    }
};
