export const enum status {
    SET_UNAUTHORIZED = 'SET_UNAUTHORIZED',
    SET_AUTHORIZED = 'SET_AUTHORIZED',
}

export const enum ToastOptions {
    info = 'info',
    success = 'success',
    warning = 'warning',
    error = 'error',
}

