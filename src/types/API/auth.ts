//* profile response *//

export type ProfileResponseType = {
    name: string,
    email: string
};

//* auth response *//

export type AuthResponseType = {
    data: string
};

//* registration request *//

export type RegistrationRequestType = {
    name: string,
    email: string,
    password: string
};

//* registration form *//

export type RegistrationFormType = RegistrationRequestType & { confirmPassword: string };

//* login request *//

export type LoginRequestType = Omit<RegistrationRequestType, 'name'>;
