//* todo response *//

export type ToDoResponseType = {
    'id': string,
    'completed': boolean,
    'title': string,
    'description': string,
    'deadline': string,
    'tag': ToDoTagResponseType,
};

type ToDoTagResponseType = {
    'id': string,
    'name': string,
    'color': string,
    'bg': string
};

//* todo request *//

export type ToDoRequestType = {
    'completed': boolean,
    'title': string,
    'description': string,
    'deadline': string,
    'tag': string
};

export type UpdateToDoRequestType = ToDoRequestType & { id: string };

export type ToDoFormType = Omit<ToDoRequestType, 'completed' | 'tag' >;
