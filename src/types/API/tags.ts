//* tags response *//

export type TagsResponseType = {
    id: string,
    name: string,
    color: string,
    bg: string,
};
