// core
import { Action } from 'history';
import { Location } from 'react-router-dom';
import { LOCATION_CHANGE } from 'redux-first-history';

export type LocationActionType = {
    type: typeof LOCATION_CHANGE,
    payload: {
        location: Location
        action: Action
    }
};
