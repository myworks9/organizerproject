// types
import { tasksTypes } from '../../lib/redux/types';
import {
    TagsResponseType, ToDoRequestType, ToDoResponseType, UpdateToDoRequestType,
} from '../API';

export type SetIsNewTaskFormOpenActionType = {
    type: tasksTypes.SET_IS_NEW_TASK_FORM_OPEN
    payload: boolean
};

export type SetTagsActionType = {
    type: tasksTypes.SET_TAGS
    payload: TagsResponseType[]
};

export type SetSelectedTagIdForRequestActionType = {
    type: tasksTypes.SET_SELECTED_TAG_ID_FOR_REQUEST
    payload: string
};

export type SetNewToDoRequestActionType = {
    type: tasksTypes.SET_NEW_TODO_REQUEST
    payload: ToDoRequestType
};

export type SetNewToDoActionType = {
    type: tasksTypes.SET_NEW_TODO
    payload: ToDoResponseType
};

export type SetSelectedToDoActionType = {
    type: tasksTypes.SET_SELECTED_TODO
    payload: ToDoResponseType | null
};

export type SetIsCreateTaskModeActionType = {
    type: tasksTypes.SET_IS_CREATE_TASK_MODE
    payload: boolean
};

export type GetAllTODOSActionType = {
    type: tasksTypes.GET_ALL_TODOS
    payload: ToDoResponseType[]
};

export type UpdateToDoActionType = {
    type: tasksTypes.UPDATE_TODO
    payload: ToDoResponseType
};

export type UpdateToDoRequestActionType = {
    type: tasksTypes.UPDATE_TODO_REQUEST
    payload: UpdateToDoRequestType
};

export type SetToDoCompleteActionType = {
    type: tasksTypes.SET_TODO_COMPLETE
    payload: boolean
};

export type DeleteToDoRequestActionType = {
    type: tasksTypes.DELETE_TODO_REQUEST
    payload: string
};

export type DeleteToDoActionType = {
    type: tasksTypes.DELETE_TODO
    payload: string
};

