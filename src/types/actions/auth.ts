// types
import { authTypes } from '../../lib/redux/types';
import { LoginRequestType, ProfileResponseType, RegistrationRequestType } from '../API';

export type SetRequestToRegistrationActionType = {
    type: authTypes.SET_REQUEST_TO_REGISTRATION
    payload: RegistrationRequestType
};

export type SetRequestToLoginActionType = {
    type: authTypes.SET_REQUEST_TO_LOGIN
    payload: LoginRequestType
};

export type SetTokenActionType = {
    type: authTypes.SET_TOKEN
    payload: string | null
};

export type SetUserProfileDataActionType = {
    type: authTypes.SET_USER_PROFILE_DATA
    payload: ProfileResponseType | null
};
