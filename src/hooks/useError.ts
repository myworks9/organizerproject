// hooks
import { useEffect } from 'react';
import { uiActions } from '../lib/redux/actions';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/reduxTypes';

// types
import { ToastOptions } from '../types';

// selectors
import { errorMessageSelector } from '../lib/redux/selectors';

// helpers
import { createToasters } from '../helpers';

export const useError = (): void => {
    const errorMessage = useAppSelector(errorMessageSelector);
    const dispatch = useAppDispatch();

    useEffect((): void => {
        if (errorMessage) {
            createToasters(ToastOptions.error, errorMessage);
            dispatch(uiActions.setError(null));
        }
    }, [errorMessage]);
};
