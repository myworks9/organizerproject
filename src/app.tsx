// Core
import { FC } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

// hooks
import { useError } from './hooks';

// Components
import {
    Footer, Login, Nav, Profile, SignUp, TaskManager,
} from './components';

// Instruments
import { book } from './book';

export const App: FC = () => {
    useError();

    return (
        <>
            <Nav />
            <main>
                <Routes >
                    <Route
                        path = { book[ '/' ] }
                        element = { <Navigate to = { book.taskManager } /> } />
                    <Route
                        path = { book.taskManager }
                        element = { <TaskManager /> } />
                    <Route
                        path = { book.profile }
                        element = { <Profile /> } />
                    <Route
                        path = { book.login }
                        element = { <Login /> } />
                    <Route
                        path = { book.signUp }
                        element = { <SignUp /> } />
                    <Route
                        path = { book[ '*' ] }
                        element = { <TaskManager /> } />
                </Routes>
            </main>
            <Footer />
        </>
    );
};

