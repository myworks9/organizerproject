// Core
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { HistoryRouter as Router } from 'redux-first-history/rr6';
import { ToastContainer, Zoom } from 'react-toastify';
import { history, store } from './lib/redux/init/store';

// Components
import { App } from './app';

// Instruments
import 'react-toastify/dist/ReactToastify.css';
import './theme/styles/index.scss';

render(
    <Provider store = { store }>
        <Router history = { history }>
            <ToastContainer newestOnTop transition = { Zoom } />
            <App />
        </Router>
    </Provider>,
    document.getElementById('root'),
    (): void => {
        // eslint-disable-next-line no-console
        console.log('%c Приложение успешно запущено ', 'background: #00ff00; color: #000000; padding: 2.5px;');
    },
);
